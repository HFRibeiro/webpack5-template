import React, { Component } from "react";
import { createRoot } from 'react-dom/client';
import config from "./config.json"

export default class App extends Component {

  socketUrl(tangoDB: string) {
    const loc = window.location;
    const protocol = loc.protocol.replace("http", "ws");
    return protocol + "//"+config.websocket+"/" + tangoDB + "/socket?dashboard";
  }

  websocket() {
    console.log("create websocket",this.socketUrl("testdb"))
    return new WebSocket(this.socketUrl("testdb"), "graphql-ws");
  }

  render() {

    const socket = this.websocket();
    const fullNames = ["sys/tg_test/1/state"];
    const ATTRIBUTES = `
    subscription Attributes($fullNames: [String]!) {
      attributes(fullNames: $fullNames) {
        device
        attribute
        value
        writeValue
        timestamp
      }
    }`;

    socket.addEventListener("open", () => {
      const variables = { fullNames };
      const startMessage = JSON.stringify({
        type: "start",
        payload: {
          query: ATTRIBUTES,
          variables
        }
      });

      socket.send(startMessage);
    });

    socket.addEventListener("message", msg => {
      const frame = JSON.parse(msg.data);
      console.log("New message: ", frame);
    });

    socket.addEventListener("close", () => {
      console.log("socket close");
    });


    return (
      <h1>React Webpack 5 with websocket ;) {" "}
        {new Date().toLocaleDateString()}</h1>
    );
  }
}

const container = document.getElementById('root') as HTMLElement;
const root = createRoot(container);
root.render(<App/>);
